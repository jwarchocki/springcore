package com.app.motor.adnotacje;

import org.springframework.stereotype.Component;

@Component
public class EngineType {
	private int cylinders;

	public int getCylinders() {
		return cylinders;
	}

	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}
}
