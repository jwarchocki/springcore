package com.app.motor.adnotacje;

import org.springframework.stereotype.Component;

@Component
public class Apperance {
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
