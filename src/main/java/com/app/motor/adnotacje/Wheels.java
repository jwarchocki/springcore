package com.app.motor.adnotacje;

import org.springframework.stereotype.Component;

@Component
public class Wheels {
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
