package com.app.motor.adnotacje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
//@Component("Motorcycle")
public class Motorcycle {
	@Autowired
	private Apperance apperance;
	@Autowired
	private Engine engine;
	@Autowired
	private Wheels wheels;
	private String owner;
	
	public Apperance getAppearance() {
		return apperance;
	}
	public void setAppearance(Apperance color) {
		this.apperance = color;
	}
	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public Wheels getWheels() {
		return wheels;
	}
	public void setWheels(Wheels wheels) {
		this.wheels = wheels;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
}
