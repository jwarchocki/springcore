package com.app.motor;

public class Motorcycle {
	private Apperance apperance;
	private Engine engine;
	private Wheels wheels;
	private String owner;
	
	public Apperance getAppearance() {
		return apperance;
	}
	public void setAppearance(Apperance color) {
		this.apperance = color;
	}
	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public Wheels getWheels() {
		return wheels;
	}
	public void setWheels(Wheels wheels) {
		this.wheels = wheels;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
}
