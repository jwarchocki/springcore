package com.app.motor;

public class EngineType {
	private int cylinders;

	public int getCylinders() {
		return cylinders;
	}

	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}
}
