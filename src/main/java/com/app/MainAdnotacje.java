package com.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.app.motor.adnotacje.Engine;
import com.app.motor.adnotacje.Motorcycle;


public class MainAdnotacje {

	static final Logger log = LogManager.getLogger(MainAdnotacje.class.getName());
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeansAnnotations.xml");
	
		Motorcycle KTM = (Motorcycle) context.getBean("motorcycle");
		
		Engine engine = (Engine) context.getBean("engine");
	//	System.out.println(KTM.getOwner());
	}
}
