package com.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.app.motor.Motorcycle;

public class Main {

	static final Logger log = LogManager.getLogger(Main.class.getName());
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		Motorcycle KTM = (Motorcycle) context.getBean("KTM");
		System.out.println(KTM.getOwner());
	}
}
